
import { TrustedRootList } from '../src/roots';
import { JsonWebCertificate } from '../src/jwc';

import { describe, expect, test } from '@jest/globals';

describe('Test the TrustedRootList', (): void => {
    test('Add and test if present', async (): Promise<void> => {
        const r = new TrustedRootList();
        const c = await JsonWebCertificate.createCaRoot();

        for (var i = 0; i < 10; i++) {
            const additionalCert = await JsonWebCertificate.createCaRoot();
            r.add(additionalCert.cert);
        }

        r.add(c.cert);

        expect(r.isTrusted(c.cert)).toBe(true);
    });

    test('Add an test an untrusted cert', async (): Promise<void> => {
        const r = new TrustedRootList();

        for (var i = 0; i < 10; i++) {
            const additionalCert = await JsonWebCertificate.createCaRoot();
            r.add(additionalCert.cert);
        }

        const notTrusted = await JsonWebCertificate.createCaRoot();

        expect(r.isTrusted(notTrusted.cert)).toBe(false);
    });
});
