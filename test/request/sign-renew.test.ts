import { describe, expect, test } from '@jest/globals';


import { JsonWebCertificateRequest, JsonWebCertificate,TrustedRootList } from '../../src';
import { createCert } from './util';


describe('Sign jwc renew request', () => {
    test('Create, validate and sign', async () => {
        const ca = await JsonWebCertificate.createCaRoot();

        const old = await createCert(ca);

        const pair = await JsonWebCertificate.createKeyPair();

        const roots = new TrustedRootList();
        roots.add(ca.cert);
        
        const req = await JsonWebCertificateRequest.createRenewRequest(pair, old);

        expect(req.isRenewRequest).toBe(true);

        // await expect(req.validate()).rejects.toBeInstanceOf(Error);
        await expect(req.validate(old.cert)).resolves.toBeUndefined();

        const nCert = await req.approve(ca);

        expect(nCert.content.subject).toEqual(old.cert.content.subject);
        expect(nCert.content.usage).toEqual(old.cert.content.usage);
        await expect(nCert.validate(roots)).resolves.toBeUndefined();
    });
});
