import { describe, expect, test } from '@jest/globals';
import { JsonWebCertificate, JsonWebCertificateRequest, Subject, Usage } from '../../src';
import { SubjectError } from '../../src/request';
import { RequestSubject } from '../../src/request_content';
import { TrustedRootList } from '../../src/roots';

describe('Test Signng of JWC Request', () => {
    test('Create & Sign', async () => {
        const ca = await JsonWebCertificate.createCaRoot();
        const roots = new TrustedRootList();
        roots.add(ca.cert);
        
        const pair = await JsonWebCertificate.createKeyPair();
        const sub: Subject = {
            commonName: 'cn',
            additional: 'additionalattribute'
        };
        const req = await JsonWebCertificateRequest.create(pair, sub, [ Usage.VERIFY ]);

        // Expect the request to be valid
        await expect(req.validate()).resolves.toBeUndefined();

        const cert = await req.approve(ca);

        // The approved cert must be valid
        await expect(cert.validate(roots)).resolves.toBeUndefined();

        expect(cert.parent).toBeDefined();
        expect(cert.parent?.content.serialNumber).toBe(ca.cert.content.serialNumber);

        expect(cert.content.usage).toContain(Usage.VERIFY);
        expect(cert.content.usage).not.toContain(Usage.VERIFY_CERTIFICATE);
        expect(cert.content.usage).not.toContain(Usage.ENCRYPT);

        expect(cert.content.subject).toEqual(sub);
    });

    test('Create, Modify content & sign', async () => {
        const ca = await JsonWebCertificate.createCaRoot();
        const roots = new TrustedRootList();
        roots.add(ca.cert);

        const pair = await JsonWebCertificate.createKeyPair();
        const sub: Subject = {
            commonName: 'cn',
            additional: 'additionalattribute'
        };

        const req = await JsonWebCertificateRequest.create(pair, sub, [ Usage.VERIFY_CERTIFICATE ]);

        // Expect the request to be valid
        await expect(req.validate()).resolves.toBeUndefined();

        const cert = await req.approve(ca, {
            overrideSubject: (s) => ({ commonName: 'ncn' }),
            overrideUsage: (u) => [ Usage.VERIFY ]
        });

        // The approved cert must be valid
        await expect(cert.validate(roots)).resolves.toBeUndefined();

        expect(cert.content.usage).toContain(Usage.VERIFY);
        expect(cert.content.usage).not.toContain(Usage.VERIFY_CERTIFICATE);
        expect(cert.content.usage).not.toContain(Usage.ENCRYPT);

        expect(cert.content.subject).toHaveProperty('commonName', 'ncn');
        expect(cert.content.subject).not.toHaveProperty('additional');
    });

    test('Test commonName generation', async () => {
        const ca = await JsonWebCertificate.createCaRoot();
        const roots = new TrustedRootList();
        roots.add(ca.cert);

        const pair = await JsonWebCertificate.createKeyPair();
        const sub: RequestSubject = {
            additional: 'additionalattribute'
        };

        const req = await JsonWebCertificateRequest.create(pair, sub);

        await expect(req.validate()).resolves.toBeUndefined();

        const cert = await req.approve(ca, {
            overrideSubject: (s) => ({ ...s,  commonName: 'ncn' })
        });

        expect(cert.content.subject).toHaveProperty('commonName', 'ncn');
        expect(cert.content.subject).toHaveProperty('additional', 'additionalattribute');
    });

    test('Fail without commonName', async () => {
        const ca = await JsonWebCertificate.createCaRoot();

        const pair = await JsonWebCertificate.createKeyPair();
        const sub: RequestSubject = {};

        const req = await JsonWebCertificateRequest.create(pair, sub);

        await expect(req.validate()).resolves.toBeUndefined();

        const p = req.approve(ca);

        await expect(p).rejects.toBeInstanceOf(SubjectError);
    });
});
