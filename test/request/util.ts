
import * as uuid from 'uuid';

import { JsonWebCertificate, CertWithKey, SignContent, Usage } from '../../src';

export async function createCert(ca: CertWithKey): Promise<CertWithKey> {
    const pair = await JsonWebCertificate.createKeyPair();

    const content: SignContent = {
        key: pair.pub,
        usage: [Usage.ENCRYPT, Usage.VERIFY],
        subject: {
            commonName: uuid.v4()
        }
    };

    const cert = await JsonWebCertificate.create(content, ca.cert, ca.priv);

    return {
        cert,
        priv: pair.priv
    };
} 