import { describe, expect, test } from '@jest/globals';
import { FlattenedJWS, GeneralJWS } from 'jose';
import { JsonWebCertificate, JsonWebCertificateRequest, Subject, Usage } from '../../src';
import { isFlatternedJws, isGeneralJws } from '../../src/request';

import { createCert } from './util';


describe('Test JWC Request Serialization', () => {
    test('Simple Request serialization',async () => {
        const pair = await JsonWebCertificate.createKeyPair();
        const sub: Subject = {
            commonName: 'cn',
            additional: 'additionalattribute'
        };
        const req = await JsonWebCertificateRequest.create(pair, sub, [ Usage.VERIFY ]);

        const json = JSON.stringify(req.toJson());
        const parsed = await JsonWebCertificateRequest.parse(JSON.parse(json));

        if(isFlatternedJws(req.signature)) {
            expect(isFlatternedJws(parsed.signature)).toBe(true);
            expect((parsed.signature as FlattenedJWS).signature).toBe(req.signature.signature);
        } else {
            expect(isGeneralJws(parsed.signature)).toBe(true);
            expect((parsed.signature as GeneralJWS).signatures.length).toBe(req.signature.signatures.length);
            expect((parsed.signature as GeneralJWS).signatures).toContainEqual(req.signature.signatures);
        }

        expect(parsed.content.usage?.length).toBe(1);
        expect(parsed.content.usage![0]).toBe(Usage.VERIFY);
        expect(parsed.content.subject?.commonName).toBe('cn');
        expect(parsed.content.subject?.additional).toBe('additionalattribute');
        expect(parsed.content.key.kid).toBe(pair.pub.kid);
        
        await parsed.validate();
    });

    test('Renew Request serialization', async () => {
        const ca = await JsonWebCertificate.createCaRoot();
        const old = await createCert(ca);
        const pair = await JsonWebCertificate.createKeyPair();
        const req = await JsonWebCertificateRequest.createRenewRequest(pair, old);

        const json = JSON.stringify(req.toJson());

        const parsed = await JsonWebCertificateRequest.parse(JSON.parse(json));

        expect(parsed.isRenewRequest).toBe(true);

        const ps = parsed.signature as GeneralJWS;
        const s = req.signature as GeneralJWS;

        expect(ps.signatures.length).toBe(s.signatures.length);

        for (const el of s.signatures) {
            expect(ps.signatures).toContainEqual(el);
        }

        expect(ps.payload).toEqual(s.payload);

        expect(parsed.content.usage?.length).toBe(req.content.usage?.length);
        for (const u of req.content.usage ?? []) {
            expect(parsed.content.usage).toContain(u);
        }
        
        expect(parsed.content.subject).toEqual(req.content.subject);

        await expect(parsed.validate(old.cert)).resolves.toBeUndefined();
    });
});
