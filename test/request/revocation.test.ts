import { describe, expect, test, jest } from '@jest/globals';

import { RevocationChecker, JsonWebCertificate, TrustedRootList, SignContent, InvalidReason, JsonWebCertificateRequest } from '../../src';

import { createCert } from './util';

describe('Test old certificate revocation', () => {
    test('Valid old cert', async () => {
        const ca = await JsonWebCertificate.createCaRoot();

        const old = await createCert(ca);

        const pair = await JsonWebCertificate.createKeyPair();

        const roots = new TrustedRootList();
        roots.add(ca.cert);
        
        const req = await JsonWebCertificateRequest.createRenewRequest(pair, old);

        const rev: RevocationChecker = {
            isRevoked: (jwc: JsonWebCertificate) => false
        };

        const spy = jest.spyOn(rev, 'isRevoked');

        await expect(req.validate(old.cert, rev)).resolves.toBeUndefined();
        expect(spy).toHaveBeenCalledTimes(1);
    });

    test('Invalid old cert', async () => {
        const ca = await JsonWebCertificate.createCaRoot();

        const old = await createCert(ca);

        const pair = await JsonWebCertificate.createKeyPair();

        const roots = new TrustedRootList();
        roots.add(ca.cert);
        
        const req = await JsonWebCertificateRequest.createRenewRequest(pair, old);

        const rev: RevocationChecker = {
            isRevoked: (jwc: JsonWebCertificate) => true
        };

        const spy = jest.spyOn(rev, 'isRevoked');

        await expect(req.validate(old.cert, rev)).rejects.toHaveProperty('reason', InvalidReason.Revoked);
        expect(spy).toHaveBeenCalledTimes(1);
    });
});