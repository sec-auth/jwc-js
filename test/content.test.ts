
import { protectedContentSchema } from '../src/content';

import { describe, expect, test  } from '@jest/globals';

describe('Test The Content data structures', (): void => {
    test('Schema Positive', ()  => {
        const o = {
            serialNumber: 'skjdghfsjgfasdgf',
            authority: 'de.mhdj.h',
            notBefore: new Date().toUTCString(),
            notAfter: new Date(Date.now() + 10000).toUTCString(),
            key: {
                kid: 'gfasjkgf',
                kty: 'EC'
            },
            subject: {
                commonName: "a name"
            },
            usage: [
                'VERIFY', 'ENCRYPT', 'VERIFY_CERTIFICATE'
            ]
        };

        const p = protectedContentSchema.required().validateAsync(o);
        return expect(p).resolves.toBeDefined();
    });

    test('Schema fail', () => {
        const o = {
            authority: 'de.mhdj.h',
            notBefore: new Date().toUTCString(),
            notAfter: new Date(Date.now() + 10000).toUTCString(),
            key: {
                kid: 'gfasjkgf'
            },
            subject: {
                commonName: "a name"
            },
            usage: [
                'VERIFY', 'ENCRYPT', 'VERIFY_CERTIFICATE'
            ]
        };

        const p = protectedContentSchema.required().validateAsync(o);

        return expect(p).rejects.toBeDefined();
    });
});