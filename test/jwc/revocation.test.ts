
import { RevocationChecker, JsonWebCertificate, TrustedRootList, SignContent, InvalidReason } from '../../src';

import { createChain } from './util';

import { describe, expect, test, jest } from '@jest/globals';

describe('Test Revocation checking', () => {
    test('Certificate chain valid', async (): Promise<void> => {

        const roots = new TrustedRootList();
        const n = 3;
        
        const { cert } = await createChain({ n, roots });

        const rev: RevocationChecker = {
            isRevoked: (jwc: JsonWebCertificate) => false
        };

        const spy = jest.spyOn(rev, 'isRevoked');

        await expect(cert.validate(roots, rev)).resolves.toBeUndefined();
        expect(spy).toHaveBeenCalledTimes(n);
    });

    test('Certificate chain invalid', async (): Promise<void> => {

        const roots = new TrustedRootList();
        const n = 3;
        
        const { cert } = await createChain({ n, roots });

        const rev: RevocationChecker = {
            isRevoked: (jwc: JsonWebCertificate) => false
        };

        const spy = jest.spyOn(rev, 'isRevoked');

        spy
            .mockReturnValueOnce(false)
            .mockReturnValueOnce(true)
            .mockReturnValueOnce(false);

        await expect(cert.validate(roots, rev)).rejects.toHaveProperty('reason', InvalidReason.Revoked);
        expect(spy).toHaveBeenCalledTimes(2);
    });

    test('Certificate chain invalid with Promises', async (): Promise<void> => {

        const roots = new TrustedRootList();
        const n = 3;
        
        const { cert } = await createChain({ n, roots });

        const rev: RevocationChecker = {
            isRevoked: (jwc: JsonWebCertificate) => false
        };

        const spy = jest.spyOn(rev, 'isRevoked');

        spy
            .mockReturnValueOnce(Promise.resolve(false))
            .mockReturnValueOnce(Promise.resolve(true))
            .mockReturnValueOnce(Promise.resolve(false));

        await expect(cert.validate(roots, rev)).rejects.toHaveProperty('reason', InvalidReason.Revoked);
        expect(spy).toHaveBeenCalledTimes(2);
    });
});
