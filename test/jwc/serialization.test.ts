import { describe, expect, test } from '@jest/globals';
import { JsonWebCertificate } from '../../src';
import { createChain } from './util';

function expectCertificate(cert: JsonWebCertificate, parsed: JsonWebCertificate) {
    expect(parsed.signature.signature).toBe(cert.signature.signature);

    if(cert.parent) {
        expect(parsed.parent).toBeDefined();
        expectCertificate(cert.parent, parsed.parent!);
    } else {
        expect(parsed.parent).toBeUndefined();
    }
}

describe('Test the Serialization of a JWC', () => {
    test('Serialize Self Signed', async () => {
        const { cert } = await JsonWebCertificate.createCaRoot();

        const json = JSON.stringify(cert.toJson());

        const parsed = await JsonWebCertificate.parse(JSON.parse(json));

        expectCertificate(cert, parsed);

    });

    test('Serialize Chain', async () => {
        const { cert } = await createChain({ n: 3 });

        const json = JSON.stringify(cert.toJson());

        const parsed = await JsonWebCertificate.parse(JSON.parse(json));

        expectCertificate(cert, parsed);

    });
});
