import { describe, expect, test } from '@jest/globals';
import { JwkKeyType } from '../../src/content';
import { JsonWebCertificate } from '../../src/jwc';

describe('Test JWC Key Generation', () => {

    test('Generate Key Pair', async () => {
        const pair = await JsonWebCertificate.createKeyPair();

        expect(pair.pub.kid).toBeDefined();
        expect(pair.pub.kty).toBe('EC');
    });

    test('Custom algorithm RS256', async () => {
        const pair = await JsonWebCertificate.createKeyPair(JwkKeyType.EC);
        expect(pair.pub.kty).toBe('EC');
    });

    test('Custom algorithm RS256', async () => {
        const pair = await JsonWebCertificate.createKeyPair(JwkKeyType.RSA);
        expect(pair.pub.kty).toBe('RSA');
    });

});
