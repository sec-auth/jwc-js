import { describe, expect, test } from '@jest/globals';

import { JsonWebCertificate } from '../../src/jwc';
import { TrustedRootList } from '../../src/roots';
import { CertJWK, ProtectedContent, SignContent, Usage } from '../../src/content';
import { InvalidReason } from '../../src/validation_result';

import { createChain, cerateCertWithContent } from './util';

import {generateKeyPair, exportJWK, JWK } from 'jose';
import { randomUUID } from "crypto";

describe('Test JWC Valiation fails', () => {
    test('Usage not allowed', async (): Promise<void> => {

        const roots = new TrustedRootList();
        
        const child = await createChain({
            n: 3,
            roots,
            mod: (content: SignContent, index: number): unknown => content.usage = [ Usage.VERIFY ]
        });

        const p = child.cert.validate(roots);

        await expect(p).rejects.toHaveProperty('reason', InvalidReason.UsageNotAllowed);

    });

    test('Untrusted Root', async (): Promise<void> => {
        const roots = new TrustedRootList();
        const child = await createChain({ n: 3 });

        const p = child.cert.validate(roots);
        await expect(p).rejects.toHaveProperty('reason', InvalidReason.UnknownParent);
    });

    test('Certificate expired', async (): Promise<void> => {
        const roots = new TrustedRootList();
        const issuer = await createChain({ n: 2, roots });

        const child = await cerateCertWithContent(
            issuer,
            (key: CertJWK): ProtectedContent => ({
                key,
                subject: { commonName: randomUUID() },
                usage: [],
                serialNumber: randomUUID(),
                notBefore: new Date('01 Jan 1970 00:00:00 GMT'),
                notAfter:  new Date('01 Jan 1971 00:00:00 GMT'),
                authority: issuer.cert.content.subject.commonName
            })
        );

        const p = child.cert.validate(roots);
        await expect(p).rejects.toHaveProperty('reason', InvalidReason.CertificateExpired);
    });

    test('Chain too long', async (): Promise<void> => {
        const roots = new TrustedRootList();
        const child = await createChain({ n: 6, roots });

        const p = child.cert.validate(roots);
        await expect(p).rejects.toHaveProperty('reason', InvalidReason.ChainTooLong);
    });
});
