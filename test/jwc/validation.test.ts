
import { describe, expect, test } from '@jest/globals';

import { JsonWebCertificate } from '../../src/jwc';
import { TrustedRootList } from '../../src/roots';
import { SignContent } from '../../src/content';

describe('Test the Validation of a JWC', () => {
    test('Validate CA Root', async (): Promise<void> => {
        const caRoot = await JsonWebCertificate.createCaRoot();
        const roots = new TrustedRootList();
        roots.add(caRoot.cert);

        expect(caRoot.cert.isSelfSigned()).toBe(true);

        await caRoot.cert.validate(roots);
    });

    test('Test a Chain of 2 Certificates', async (): Promise<void> => {

        const caRoot = await JsonWebCertificate.createCaRoot();
        const roots = new TrustedRootList();
        roots.add(caRoot.cert);

        const { pub } = await JsonWebCertificate.createKeyPair();

        const content: SignContent = {
            key: pub,
            subject: {
                commonName: 'sdfhsadjfhajksdfhkjasdfh2313'
            },
            usage: []
        };

        const cert = await JsonWebCertificate.create(content, caRoot.cert, caRoot.priv);

        await cert.validate(roots);
    });
});
