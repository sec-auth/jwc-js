
import { TrustedRootList } from '../../src/roots';
import { JsonWebCertificate, CertWithKey } from '../../src/jwc';
import { SignContent, ProtectedContent, Usage, CertJWK } from '../../src/content';

import { generateKeyPair, exportJWK, JWK, FlattenedSign } from 'jose';
import { randomUUID } from "crypto";

interface ContentModifier {
    (content: SignContent, index: number): void;
}

interface CreateChainOps { 
    n?: number;
    roots?: TrustedRootList; 
    mod?: ContentModifier;
}

export async function createChain(ops: CreateChainOps = {}): Promise<CertWithKey> {

    const n = ops.n || 3;

    let current = await JsonWebCertificate.createCaRoot();
    ops.roots?.add(current.cert);

    for (let i = 1; i < n; i++) {
        const pair = await JsonWebCertificate.createKeyPair();
        
        const content: SignContent = {
            key: pair.pub,
            subject: {
                commonName: randomUUID()
            },
            usage: [Usage.VERIFY_CERTIFICATE]
        };

        if(ops.mod) {
            ops.mod(content, i);
        }

        const cert = await JsonWebCertificate.create(
            content, 
            current.cert, 
            current.priv
        );

        current = {
            priv: pair.priv,
            cert
        };
    }

    return current;

}

interface ContentFactory {
    (key: CertJWK): ProtectedContent;
}

export async function cerateCertWithContent(parent: CertWithKey, contentFactory: ContentFactory): Promise<CertWithKey> {

    const pair = await JsonWebCertificate.createKeyPair();

    const content = contentFactory(pair.pub);

    const encoder = new TextEncoder();
    const signer = new FlattenedSign(encoder.encode(JSON.stringify(content)));
    signer.setProtectedHeader({ 
        alg: JsonWebCertificate.contentSignAlgorithm(parent.cert.content)
    });
    signer.setUnprotectedHeader({ kid: parent.cert.content.key.kid });

    const signature = await signer.sign(parent.priv);

    return {
        cert: new JsonWebCertificate(signature, content, parent.cert),
        priv: pair.priv
    };
}
