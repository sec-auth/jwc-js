
import { JWK } from 'jose';
import * as Joi from 'joi';

export enum JwkKeyType {
    EC = "EC",
    RSA = "RSA"
}

// A Special JWK interface where kid and kty must be present
export interface CertJWK extends JWK {
    kty: JwkKeyType;
    kid: string;
}

export interface SignContent {
    key:          CertJWK;
    subject:      Subject;
    usage:        Usage[];
}

export interface ProtectedContent extends SignContent {
    serialNumber: string;
    notBefore:    Date;
    notAfter:     Date;
    authority:    string;
}

export const jwkMinSchema = Joi.object({
    kid: Joi.string()
        .required(),

    kty: Joi.string()
        .required()
        .valid(JwkKeyType.RSA, JwkKeyType.EC)
}).unknown();



export interface Subject {
    commonName: string;
    [name: string]:  string;
}

export const subjectSchema = Joi.object({
    commonName: Joi.string()
        .required()
}).pattern(/./, Joi.string());

export enum Usage {
    VERIFY = 'VERIFY',
    ENCRYPT = 'ENCRYPT',
    VERIFY_CERTIFICATE = 'VERIFY_CERTIFICATE'
}

export const usageSchema = Joi.string()
    .valid(Usage.VERIFY, Usage.ENCRYPT, Usage.VERIFY_CERTIFICATE);

export const usageArraySchema = Joi.array().items(usageSchema).max(3);

export const protectedContentSchema = Joi.object({
    serialNumber: Joi.string()
        .required(),
    
    authority: Joi.string()
        .required(),

    notBefore: Joi.date()
        .required(),

    notAfter: Joi.date()
        .required(),

    key: jwkMinSchema
        .required(),

    subject: subjectSchema
        .required(),
    
    usage: Joi.array()
        .items(
            usageSchema
        )
        .required()
        .max(3)
});

