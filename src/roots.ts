
import { JsonWebCertificate } from './jwc';
import * as sha256 from 'crypto-js/sha256';

export interface TrustedRoots {
    isTrusted(cert: JsonWebCertificate): boolean;
}

export class TrustedRootList implements TrustedRoots {
    private roots: Array<string> = [];

    private hash(jwc: JsonWebCertificate): string {
        return sha256(jwc.signature.payload).toString();
    }

    add(jwc: JsonWebCertificate) {
        this.roots.push(this.hash(jwc));
    }

    isTrusted(jwc: JsonWebCertificate): boolean {
        return this.roots.includes(this.hash(jwc));
    }
}
