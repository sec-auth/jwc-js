
export enum InvalidReason{
    UnexpectedError,
    InvalidSignature,
    InvalidKey,
    ChainTooLong,
    CertificateExpired,
    UnknownParent,
    UsageNotAllowed,
    OldCertificateSignatureNotProvided,
    Revoked
}

export class ValidationError implements Error {
    reason: InvalidReason;
    exception?: unknown;

    constructor (reason: InvalidReason = InvalidReason.UnexpectedError, e?: unknown) {
        this.reason = reason;
        this.exception = e;
    }

    get name (): string{
        return "ValidationError";
    }
    
    get message(): string {
        let s = `ValidationError: The JsonWebCertificate is invalid because: ${InvalidReason[this.reason]}`;
        if (this.exception && this.exception instanceof Error) {
            s += `; Caused by: ${this.exception.name}: ${this.exception.message}`;
        } else if (this.exception) {
            s += `; Caused by: ${this.exception.toString()}`;
        }
        return s;
    }
}
