
import { jwkMinSchema, usageArraySchema, CertJWK, Usage } from './content';

import * as Joi from 'joi';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface RequestContent {
    key:          CertJWK;
    subject?:      RequestSubject;
    usage?:        Usage[];
}

export interface RequestSubject {
    [x: string]: string;
}

export const requestSubjectSchema = Joi.object().pattern(/./, Joi.string());

export const requestContentSchema = Joi.object({
    key: jwkMinSchema.required(),
    subject: requestSubjectSchema,
    usage: usageArraySchema,
});
