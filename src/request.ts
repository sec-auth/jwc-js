
import { RequestContent, requestContentSchema, RequestSubject } from './request_content';
import { JsonWebCertificate, CertWithKey, KeyPair, RevocationChecker } from './jwc';
import { InvalidReason, ValidationError } from './validation_result';
import { Usage, SignContent, Subject } from './content';
import { jsonDecodeSignaturePayload } from './util';

import { FlattenedJWS, flattenedVerify, importJWK, FlattenedSign, decodeProtectedHeader, GeneralJWS, JWSHeaderParameters, generalVerify, base64url, GeneralSign } from 'jose';
import * as Joi from 'joi';

const base64UrlString = Joi.string().base64({ urlSafe: true, paddingRequired: false });

const joseHeaderSchema = Joi.object({
    alg: Joi.string(),

    kid: Joi.string()
        .required()
});

const generalJwsSignaturesSchema = Joi.array()
    .min(2)
    .items(Joi.object({
        signature: base64UrlString.required(),

        protected: base64UrlString.required(),

        header: joseHeaderSchema.required()
    }));

export const flatternedOrGeneralJwsSchema = Joi.object({
        // Payload is present on Flatterned and General JWS
        payload: base64UrlString.required(),

        signatures: generalJwsSignaturesSchema,

        signature: base64UrlString,
        
        header: joseHeaderSchema,
            
        protected: base64UrlString
    })
    .and('signature', 'header', 'protected')
    .xor('signature', 'signatures');

export function isGeneralJws(o: FlattenedJWS | GeneralJWS): o is GeneralJWS {
    return 'signatures' in o;
}

export function isFlatternedJws(o: FlattenedJWS | GeneralJWS): o is FlattenedJWS {
    return 'signature' in o;
}

export class JsonWebCertificateRequest {

    signature: FlattenedJWS | GeneralJWS;
    content: RequestContent;

    constructor(signature: FlattenedJWS | GeneralJWS, content: RequestContent) {
        this.signature = signature;
        this.content = content;
    }

    get isRenewRequest (): boolean {
        return isGeneralJws(this.signature);
    }

    static async parse(input: unknown): Promise<JsonWebCertificateRequest> {
        const signature = await flatternedOrGeneralJwsSchema.required().validateAsync(input) as FlattenedJWS | GeneralJWS;

        const unsecureContent = jsonDecodeSignaturePayload(signature);
        const content = await requestContentSchema.required().validateAsync(unsecureContent) as RequestContent;

        return new JsonWebCertificateRequest(signature, content);
    }

    static async create(pair: KeyPair, subject?: RequestSubject, usage?: Usage[]): Promise<JsonWebCertificateRequest> {
        const content: RequestContent = {
            usage,
            subject,
            key: pair.pub
        };

        const signer = new FlattenedSign(new TextEncoder().encode(JSON.stringify(content)));
        signer.setProtectedHeader({ 
            alg: JsonWebCertificate.contentSignAlgorithm(content)
        });
        signer.setUnprotectedHeader({ 
            kid: content.key.kid 
        });

        const signature = await signer.sign(pair.priv);

        return new JsonWebCertificateRequest(signature, content);
    }

    static async createRenewRequest(pair: KeyPair, old: CertWithKey, subject?: RequestSubject, usage?: Usage[]): Promise<JsonWebCertificateRequest> {
        const content: RequestContent = {
            usage: usage ?? old.cert.content.usage,
            subject: subject ?? old.cert.content.subject,
            key: pair.pub
        };

        const signer = new GeneralSign(new TextEncoder().encode(JSON.stringify(content)))
            .addSignature(pair.priv)
                .setProtectedHeader({ alg: JsonWebCertificate.contentSignAlgorithm(content) })
                .setUnprotectedHeader({ kid: content.key.kid })
            .addSignature(old.priv)
                .setProtectedHeader({ alg: JsonWebCertificate.contentSignAlgorithm(old.cert.content) })
                .setUnprotectedHeader({ kid: old.cert.content.key.kid });

        const signature = await signer.sign();
        
        return new JsonWebCertificateRequest(signature, content);
    }   

    async validate(oldJwc?: JsonWebCertificate, rev?: RevocationChecker): Promise<void> {
        if(isFlatternedJws(this.signature)) {
            if(oldJwc) throw new ValidationError(InvalidReason.OldCertificateSignatureNotProvided);

            await this.validateSelfSignature({
                ...decodeProtectedHeader(this.signature),
                ...this.signature.header
            });
        } else {
            const hSelf = this.getSignatureHeaderForKid(this.signature, this.content.key.kid);
            await this.validateSelfSignature(hSelf);

            // Add an else with throw?
            if(oldJwc) {
                const hOld = this.getSignatureHeaderForKid(this.signature, oldJwc.content.key.kid);
                // TODO validate the oldCert?
                await this.validateOldSignature(oldJwc, this.signature, hOld, rev);
            }
        }
    }

    private getSignatureHeaderForKid(sig: GeneralJWS, kid: string): JWSHeaderParameters {
        for(const s of sig.signatures) {
            const p = s.protected ? JSON.parse(new TextDecoder().decode(base64url.decode(s.protected))) as JWSHeaderParameters: {};

            const h = {
                ...p,
                ...s.header
            };

            if(h.kid == kid) return h;
        }

        throw new ValidationError(InvalidReason.InvalidSignature);
    }

    private async validateOldSignature(old: JsonWebCertificate, sig: GeneralJWS, h: JWSHeaderParameters, rev?: RevocationChecker): Promise<void> {
        if(rev && await rev.isRevoked(old)) {
            throw new ValidationError(InvalidReason.Revoked);
        }
        
        try {
            if(h?.alg == undefined) throw new Error('Signature Algorithm could not be read from Header or Protected Header');

            const key = await importJWK(old.content.key, h.alg);
            await generalVerify(sig, key);
        } catch(e: unknown) {
            throw new ValidationError(InvalidReason.InvalidSignature, e);
        }
    }

    private async validateSelfSignature(h: JWSHeaderParameters): Promise<void> {
        try {
            if(h.alg == undefined) throw new Error('Signature Algorithm could not be read from Header or Protected Header');

            const key = await importJWK(this.content.key, h.alg);
            if(isFlatternedJws(this.signature)) {
                await flattenedVerify(this.signature, key);
            }
            else {
                await generalVerify(this.signature, key);
            }
            
        } catch(e: unknown) {
            throw new ValidationError(InvalidReason.InvalidSignature, e);
        }
    }

    private mapSubjetc(overrideSubject?: (s: RequestSubject) => Subject): Subject {
        if(overrideSubject) {
            return overrideSubject(this.content.subject ?? {});
        }
        else if(this.content.subject?.commonName) {
            return {
                commonName: this.content.subject?.commonName,
                ...this.content.subject
            };
        } else {
            throw new SubjectError();
        }
    }

    private mapUsage(overrideUsage?: Overrider<Usage[]>): Usage[]{
        overrideUsage ??= u => u;
        return overrideUsage(this.content.usage ?? []);
    }

    async approve(parent: CertWithKey, ops: ApproveOptions = {}): Promise<JsonWebCertificate> {
        
        const c: SignContent = {
            key: this.content.key,
            subject: this.mapSubjetc(ops.overrideSubject),
            usage: this.mapUsage(ops.overrideUsage)
        };

        return await JsonWebCertificate.create(c,parent.cert, parent.priv);
    }


    toJson(): unknown {
        return this.signature;
    }

}

export class SubjectError implements Error{
    
    readonly name: string = 'SubjectError';
    readonly message: string = 'The Subject is missing required fields';

}

export interface Overrider<T> {
    (obj: T): T;
}

export interface ApproveOptions {
    overrideSubject?: (s: RequestSubject) => Subject;
    overrideUsage?: Overrider<Usage[]>;
}
