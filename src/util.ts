
import { FlattenedJWS, base64url, GeneralJWS } from 'jose';

export function jsonDecodeSignaturePayload(signature: FlattenedJWS | GeneralJWS): unknown {
    const base64String = signature.payload;
    const bytes = base64url.decode(base64String);
    const jsonString = new TextDecoder().decode(bytes);
    return JSON.parse(jsonString) as unknown;
}
