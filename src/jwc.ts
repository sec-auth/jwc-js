
import { ProtectedContent, Usage, protectedContentSchema, SignContent, JwkKeyType, CertJWK } from './content';
import { FlattenedJWS, flattenedVerify, importJWK, FlattenedSign, KeyLike, generateKeyPair, exportJWK, decodeProtectedHeader } from 'jose';
import { ValidationError, InvalidReason } from './validation_result';
import { TrustedRoots } from './roots';
import { v4 as uuidv4 } from 'uuid';
import { jsonDecodeSignaturePayload } from './util';

import * as Joi from 'joi';
import { RequestContent } from './request_content';

export interface JWC {
    signature: FlattenedJWS;
    parent?:   JWC;
}

export interface CertWithKey { 
    cert: JsonWebCertificate;
    priv: Uint8Array | KeyLike;
}

export interface KeyPair {
    priv: Uint8Array | KeyLike;
    pub: CertJWK;
}

export const minJwsSchema = Joi.object({
    payload: Joi.string()
        .base64({ urlSafe: true, paddingRequired: false })
        .required(),

    header: Joi.object({
            alg: Joi.string(),

            kid: Joi.string()
                .required()
        })
        .required()
}).unknown();

export const jwcSchema = Joi.object({
    signature: minJwsSchema
        .required(),

    parent: Joi.object() //TODO besser machen
});

export interface RevocationChecker {
    isRevoked(jwc: JsonWebCertificate): Promise<boolean> | boolean;
}

export class JsonWebCertificate implements JWC{

    signature: FlattenedJWS;
    parent?:   JsonWebCertificate;
    content:   ProtectedContent;

    constructor (signature: FlattenedJWS, content: ProtectedContent, parent?: JsonWebCertificate) {
        this.signature = signature;
        this.content = content;
        this.parent = parent;
    }

    private static createContent(content: SignContent, authority: string): ProtectedContent {
        const notBefore = new Date();
        const notAfter = new Date();
        notAfter.setFullYear(notAfter.getFullYear() + 4);
        return {
            ...content,
            serialNumber: uuidv4(),
            notBefore,
            notAfter,
            authority
        };
    }

    static async create(content: SignContent, parent: JsonWebCertificate, priv: Uint8Array | KeyLike): Promise<JsonWebCertificate> {
        const c = this.createContent(content, parent.content.subject.commonName);

        const alg = parent.signAlgorith();

        const encoder = new TextEncoder();
        const signer = new FlattenedSign(encoder.encode(JSON.stringify(c)));
        signer.setProtectedHeader({ alg });
        signer.setUnprotectedHeader({ kid: parent.content.key.kid });

        const signature = await signer.sign(priv);

        return new JsonWebCertificate(signature, c, parent);
    }

    static async createSelfSigned(content: SignContent, priv: Uint8Array | KeyLike, authority: string): Promise<JsonWebCertificate> {
        const c = this.createContent(content, authority);

        const alg = this.contentSignAlgorithm(c);

        const encoder = new TextEncoder();
        const signer = new FlattenedSign(encoder.encode(JSON.stringify(c)));
        signer.setProtectedHeader({ alg });
        signer.setUnprotectedHeader({ kid: content.key.kid });

        const signature = await signer.sign(priv);

        return new JsonWebCertificate(signature, c);
    }

    static async createKeyPair(kty?: JwkKeyType): Promise<KeyPair> {
        let alg: string;
        kty ??=  JwkKeyType.EC;
        switch (kty) {
            case JwkKeyType.EC:
                alg = 'ES512'; break;
            case JwkKeyType.RSA: 
                alg = 'RS512'; break;
            // default: throw new Error(`Unknown kty ${kty}`);
        }

        const { publicKey, privateKey } = await generateKeyPair(alg);
        const key = await exportJWK(publicKey);
        
        return {
            priv: privateKey,
            pub: {
                ...key,
                kty,
                kid: uuidv4()
            }
        };
    }

    static async createCaRoot(ops?: {kty?: JwkKeyType, authority?: string}): Promise<CertWithKey> {
        const authority = ops?.authority || uuidv4();

        const kPair = await this.createKeyPair(ops?.kty);

        const c: SignContent = {
            key: kPair.pub,
            subject: {
                commonName: authority
            },
            usage: [
                Usage.VERIFY_CERTIFICATE, Usage.VERIFY, Usage.ENCRYPT
            ]
        };

        const cert = await this.createSelfSigned(c, kPair.priv, authority);

        return {
            cert,
            priv: kPair.priv
        };
    }

    static contentSignAlgorithm(content: SignContent | RequestContent): string {
        if(content.key.kty == JwkKeyType.EC) return 'ES512';
        if(content.key.kty == JwkKeyType.RSA) return 'RS256';
        // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
        throw new Error(`Cannot determine sign algorith for kty ${content.key.kty}`);
    }

    signAlgorith(): string {
        return JsonWebCertificate.contentSignAlgorithm(this.content);
    }

    static async parse(input: unknown): Promise<JsonWebCertificate> {
        const cert = await jwcSchema.required().validateAsync(input) as JWC;
        const signature = cert.signature;
        const parent = cert.parent ? await JsonWebCertificate.parse(cert.parent) : undefined;

        const unsecureContent = jsonDecodeSignaturePayload(signature);
        const content = await protectedContentSchema.required().validateAsync(unsecureContent) as ProtectedContent;

        return new JsonWebCertificate(signature, content, parent);
    }

    isSelfSigned(): boolean {
        return this.content.key.kid !== undefined && this.content.key.kid === this.signature.header?.kid;
    }

    async validate(roots: TrustedRoots, rev?: RevocationChecker, step = 0): Promise<void> {
        if(step > 4) {
            throw new ValidationError(InvalidReason.ChainTooLong);
        }

        if(this.isExpired()) {
            throw new ValidationError(InvalidReason.CertificateExpired);
        }

        if (this.isSelfSigned()) { 
            await this.validateSelfSigned(roots, rev);
        } else if (this.parent) {
            await this.validateWith(this.parent, rev);
            await this.parent.validate(roots, rev, step + 1);
        } else {
            throw new ValidationError(InvalidReason.UnknownParent);
        }
    }

    private async validateSelfSigned(roots: TrustedRoots, rev?: RevocationChecker): Promise<void> {
        await this.validateWith(this, rev);
        if(!roots.isTrusted(this)) {
            throw new ValidationError(InvalidReason.UnknownParent);
        }
    }

    isAllowedToIssue(): boolean {
        return this.content.usage.includes(Usage.VERIFY_CERTIFICATE);
    }

    isExpired(instant?: Date): boolean {
        const now = instant || new Date();

        return now < this.content.notBefore || now > this.content.notAfter;
    }

    private async validateWith(parent: JsonWebCertificate, rev?: RevocationChecker): Promise<void> {
        if (! parent.isAllowedToIssue()) {
            throw new ValidationError(InvalidReason.UsageNotAllowed);
        }

        if (rev && await rev.isRevoked(this)) {
            throw new ValidationError(InvalidReason.Revoked);
        }
        
        try {
            const protectedHeaders = decodeProtectedHeader(this.signature);
            const alg = protectedHeaders.alg || this.signature.header?.alg;

            if(alg == undefined) throw new Error('Signature Algorithm could not be read from Header or Protected Header');

            const key = await importJWK(parent.content.key, alg);
            await flattenedVerify(this.signature, key);
        } catch(e: unknown) {
            throw new ValidationError(InvalidReason.InvalidSignature, e);
        }
    }

    toJson(): JWC {
        return {
            signature: this.signature,
            parent: this.parent?.toJson()
        };
    }

}
