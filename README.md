
This Package implements JSON Web Certificates. 

# Create a CA root

```typescript
    // Create a defualt CA Root with a new Jey
    let ca = await JsonWebCertificate.createCaRoot();

    // Send the certificate as json
    let json = JSON.stringify(ca.cert.toJson());
```

# Sign a certificate request

```typescript

    // Signing certificate with private key from somewhere...
    let ca = ...

    // Parse the request
    let request = await JsonWebCertificateRequest.parse(JSON.parse(jsonStringFromSomewhere));

    // Validate the request
    await request.validate();

    let certificate = await request.approve(ca);
```
