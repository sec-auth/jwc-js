# 0.3.0

Added Support for external revocation checking while validating JWCs and JWS Requests.

# 0.1.4

JWC Requests are less strict. The subject and the usage is optional now to allow the issuer to generate that values.

# 0.0.3, 0.0.4

Small Test-Improvements

# 0.0.1

Initial Version
